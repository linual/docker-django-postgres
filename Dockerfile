FROM python:alpine3.15

WORKDIR /usr/src/app
RUN apk update && apk add libpq-dev libc-dev gcc
COPY web/requirements.txt requirements.txt
RUN pip install holdup \
    pip install -r requirements.txt
